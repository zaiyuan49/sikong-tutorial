package com.sikong.hadoop;

import com.sikong.configuration.Constant;
import com.sikong.hdfs_tutorial.HdfsClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class Hdfs {

    private HdfsClient hdfs = new HdfsClient();

    @Before
    public void mkConnection() throws URISyntaxException, IOException, InterruptedException {
        hdfs.mkConnection(Constant.HADOOP_HOST,Constant.USER_NAME);
    }

    @After
    public void disConnection() throws IOException {
        hdfs.disConnection();
    }
    @Test
    public void mkDir() throws IOException {
        hdfs.mkDir("/APITest");
    }

    @Test
    public void delDir() throws IOException {
        hdfs.delDir("/APITest");
    }

    @Test
    public void putFile() throws IOException {
        hdfs.putFile("src/main/resources/word.txt","/APITest/word.txt");
    }

    @Test
    public void getFile() throws IOException {
        hdfs.getFile("/APITest/word.txt","src/test/resources/word.txt");
    }

    @Test
    public void getInfo() throws IOException {
        hdfs.getInfo("/APITest/word.txt");
    }

}
