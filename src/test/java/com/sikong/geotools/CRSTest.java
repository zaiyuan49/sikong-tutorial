package com.sikong.geotools;

import com.sikong.geotools.geometryCRS.CRSLab;

import java.io.IOException;

public class CRSTest {
    public static void main(String[] args) throws IOException {
        CRSLab lab = new CRSLab();
        lab.displayShapefile();
    }
}
