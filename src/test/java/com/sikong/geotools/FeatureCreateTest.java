package com.sikong.geotools;

import com.sikong.geotools.simple.FeatureCreate;
import org.geotools.feature.SchemaException;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeatureType;

public class FeatureCreateTest {

    public static void main(String[] args) throws SchemaException {
        FeatureCreate demo = new FeatureCreate();

        SimpleFeatureType featureType = demo.mkFeatureType();
        Feature feature = demo.mkFeature(featureType);

        System.out.println(feature);
        System.out.println(feature.getProperties("location"));//获取属性对应信息
    }
}
