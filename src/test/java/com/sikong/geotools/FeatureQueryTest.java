package com.sikong.geotools;

import com.sikong.geotools.simple.FeatureQuery;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.filter.text.cql2.CQLException;
import org.opengis.feature.simple.SimpleFeature;

import java.io.IOException;

public class FeatureQueryTest {
    public static void main(String[] args) throws IOException, CQLException {
        FeatureQuery demo = new FeatureQuery();

        SimpleFeatureSource featureSource = demo.readShp("src/main/resources/buildingData/长沙.shp");
        SimpleFeatureCollection featureCollection = demo.queryShp(featureSource,"Floor=50");

        /**
         * featureCollection的size即为查询到的结果数量
         */
        System.out.println(featureCollection.size());

        SimpleFeatureIterator iterator = featureCollection.features();

        /**
         * 利用while循环打印出查询结果
         * Geotools中编号计数由1开始，shp中编号由0开始
         */
        while (iterator.hasNext()) {
            SimpleFeature feature =  iterator.next();
            System.out.println(feature.getID());
            System.out.println(feature.getProperties("Id"));
        }
    }
}
