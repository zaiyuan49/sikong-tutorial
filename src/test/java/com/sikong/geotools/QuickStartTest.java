package com.sikong.geotools;


import com.sikong.geotools.quickstart.Mapshow;

import javax.swing.*;
import java.io.IOException;


public class QuickStartTest {

    public static void main(String[] args) throws IOException, UnsupportedLookAndFeelException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        Mapshow quickstart = new Mapshow();
        quickstart.showByDisk();
    }
}
