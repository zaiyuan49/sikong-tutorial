package com.sikong.geotools;

import com.sikong.geotools.featureTutorial.Csv2shp;

public class Csv2shpTest {

    public static void main(String[] args) throws Exception {
        Csv2shp client = new Csv2shp();
        client.setTypeName("Location");
        client.setTypeSpec("the_geom:Point:srid = 4326,name:String,number:Integer");
        client.selectInputCSV();
        client.wri2SHP();
    }

}
