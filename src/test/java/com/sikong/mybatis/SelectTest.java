package com.sikong.mybatis;

import com.sikong.mybatis.mapper.SelectMapper;
import com.sikong.mybatis.pojo.User;
import com.sikong.mybatis.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 不同查询功能的测试
 */
public class SelectTest {

    @Test
    public void selectTest1() {
        SelectMapper mapper = getSession();
        List<User> userList = mapper.getUserList();
        userList.forEach(System.out::println);
    }

    @Test
    public void selectTest2() {
        SelectMapper mapper = getSession();
        System.out.println(mapper.getCount());
    }

    @Test
    public void selectTest3() {
        SelectMapper mapper = getSession();
        Map<String, Object> userToMap = mapper.getUserToMap(1);
        System.out.println(userToMap);
    }

    @Test
    public void selectTest4() {
        SelectMapper mapper = getSession();
        Map<String, Object> allUserToMap = mapper.getAllUserToMap();
        System.out.println(allUserToMap);
    }



    public SelectMapper getSession() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        SelectMapper mapper = sqlSession.getMapper(SelectMapper.class);
        return mapper;
    }
}
