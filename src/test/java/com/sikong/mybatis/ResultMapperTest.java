package com.sikong.mybatis;

import com.sikong.mybatis.mapper.ResultMapper;
import com.sikong.mybatis.mapper.StepMapper;
import com.sikong.mybatis.pojo.Athlete;
import com.sikong.mybatis.pojo.Country;
import com.sikong.mybatis.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 利用resultMap功能的测试
 */
public class ResultMapperTest {

    /**
     * 测试resultMap基本功能
     */
    @Test
    public void test1() {
        ResultMapper session = getSession();
        List<Athlete> allAthlete = session.getAllAthlete();
        allAthlete.forEach(System.out::println);
    }

    /**
     * 多对一：分布查询
     */
    @Test
    public void test2() {
        ResultMapper session = getSession();
        System.out.println(session.getAthleteStepOne(1));
    }

    /**
     * 一对多：分布查询
     */
    @Test
    public void test3() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        StepMapper mapper = sqlSession.getMapper(StepMapper.class);
        Country athsStepOne = mapper.getAthsStepOne(1);
        System.out.println(athsStepOne);

    }

    public ResultMapper getSession() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        ResultMapper mapper = sqlSession.getMapper(ResultMapper.class);
        return mapper;
    }
}
