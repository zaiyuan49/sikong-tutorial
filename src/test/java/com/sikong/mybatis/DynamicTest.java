package com.sikong.mybatis;

import com.sikong.mybatis.mapper.DynamicMapper;
import com.sikong.mybatis.pojo.User;
import com.sikong.mybatis.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: SQL拼接的测试
 */
public class DynamicTest {

    /**
     * 测试if标签
     */
    @Test
    public void test1() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        DynamicMapper mapper = sqlSession.getMapper(DynamicMapper.class);
        List<User> wang = mapper.getUserByIf(new User(null, "Wang", null, null));
        wang.forEach(System.out::println);
    }

    /**
     * 测试choose when标签
     */
    @Test
    public void test2() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        DynamicMapper mapper = sqlSession.getMapper(DynamicMapper.class);
        List<User> userByChoose = mapper.getUserByChoose(null);
        userByChoose.forEach(System.out::println);
    }

    /**
     * 测试foreach标签
     */
    @Test
    public void test3() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        DynamicMapper mapper = sqlSession.getMapper(DynamicMapper.class);
        User user1 = new User(null, "jobs", "apple", "m");
        User user2 = new User(null, "alice", "alice", "f");
        List<User> users = Arrays.asList(user1, user2);
        System.out.println(mapper.insertByList(users));
    }
}
