package com.sikong.mybatis;

import com.sikong.mybatis.mapper.SpecialMapper;
import com.sikong.mybatis.pojo.User;
import com.sikong.mybatis.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 一些特殊sql的测试
 */
public class SpecialTest {

    @Test
    public void testMohu() {
        SpecialMapper session = getSession();
        List<User> an = session.getUserByLike("an");
        an.forEach(System.out::println);
    }
    @Test
    public void testDelete() {
        SpecialMapper mapper = getSession();
        System.out.println(mapper.deleteMore("3,4"));
    }

    @Test
    public void getTable() {
        SpecialMapper session = getSession();
        List<User> t_user = session.getUserByTable("t_user");
        for (User user : t_user) {
            System.out.println(user);
        }
    }

    public SpecialMapper getSession() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        SpecialMapper mapper = sqlSession.getMapper(SpecialMapper.class);
        return mapper;
    }
}
