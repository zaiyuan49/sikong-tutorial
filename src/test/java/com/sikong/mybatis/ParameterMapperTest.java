package com.sikong.mybatis;

import com.sikong.mybatis.mapper.UserMapper;
import com.sikong.mybatis.pojo.User;
import com.sikong.mybatis.utils.SqlSessionUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 传入参数的测试
 */
public class ParameterMapperTest {

    /**
     * 获取参数值两种方式
     * ${}的本质就是字符串拼接，#{}的本质就是占位符赋值
     * 1.参数接口单个字面量
     * 2.参数为多个
     * 3.参数为实体类的对象
     * 4.命名参数（注解 @Param）
     */
    @Test
    public void testSelect() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        Map<String, Object> map = new HashMap<>();
        map.put("user_name", "Wang");
        map.put("password", "moron_idiot");
        User wang = mapper.checkLoginByMap(map);
        System.out.println(wang);
    }

    @Test
    public void testSelectByParam() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User wang = mapper.checkLoginByParam("HebeTien", "tfz");
        System.out.println(wang);
    }

    @Test
    public void testInsert() {
        SqlSession sqlSession = SqlSessionUtils.getSqlSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        User user = new User(null, "HebeTien", "tfz", "f");
        System.out.println(mapper.insertUserWithParam(user));
    }

}
