package com.sikong;

import com.alibaba.fastjson.JSON;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.net.URLDecoder;
import java.sql.*;
import java.util.*;

/**
 * Calcite实例测试
 *
 * @author wangbohong
 * @date 2022/03/09 16:26:22
 * @since 0.1.0
 */
public class CsvCalciteTest {

    Properties config;

    @Before
    public void init() throws Exception {
        URL url = this.getClass().getResource("/model.json");
        assert url != null;
        String str = URLDecoder.decode(url.toString(), "UTF-8");
        config = new Properties();
        config.put("model", str.replace("file:", ""));
        config.put("caseSensitive", "false");
    }

    @Test
    public void testQuery() throws Exception {
        try (Connection connect = DriverManager.getConnection("jdbc:calcite:", config);
             Statement statement = connect.createStatement();
             ResultSet resultSet = statement.executeQuery("select * from TEST_CSV.TEST01");) {
            Assert.assertEquals(JSON.toJSONString(getData(resultSet)),
                    "[{\"ID\":\"0\",\"NAME\":\"jack\",\"COMPANY\":\"alibaba\"},{\"ID\":\"1\",\"NAME\":\"pony\",\"COMPANY\":\"tencent\"}]");
        }
    }


    public List<Map<String, Object>> getData(ResultSet resultSet) throws Exception {
        List<Map<String, Object>> list = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnSize = metaData.getColumnCount();
        while (resultSet.next()) {
            Map<String, Object> map = new HashMap<>();
            for (int i = 1; i < columnSize + 1; i++) {
                map.put(metaData.getColumnLabel(i), resultSet.getObject(i));
            }
            list.add(map);
        }
        return list;
    }
}