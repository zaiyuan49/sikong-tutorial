package com.sikong.hbase;

import com.sikong.hbase_tutorial.HBaseClient;

import java.io.IOException;

public class HBase {
    public static void main(String[] args) throws IOException {
        HBaseClient client = new HBaseClient();
        /*连接至HBase*/
        client.mkConnection();

        /*在HBase中创建命名空间*/
//        client.creatNameSpace("apiTest");

        /*在HBase中创建表*/
//        client.creatTable("apiTestTable",1,"info");

        /*在HBase中插入一行数据*/
//        client.dataInsert("apiTestTable","001","info","this is a test data");

        /*在HBase扫描一张表的数据*/
        client.dataCheck("ArchiLab_ingestdemo_id_v4");

        /*在HBase中删除一张表*/
//        client.deleteTable("apiTestTable");

        /*在HBase中删除命名空间*/
//        client.deleteNameSpace("apiTest");

        /*与HBase断开连接*/
        client.disConnection();
    }
}
