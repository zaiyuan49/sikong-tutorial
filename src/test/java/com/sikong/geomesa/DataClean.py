from com.sikong.geomesa.CSVParse import GetMongoData
import os

if __name__ == '__main__':

    demo = GetMongoData(collection="GuangZhou",dataBase="Weibo_DATA")
    data = demo.extractDB()
    del data['_id']
    dataTransformed = demo.dataTrasnform(data=data)
    dataCleaned = demo.dataClean(dataTransformed)

    #不知道为什么用不了相对路径，为防止泄露隐私，利用os模块构建路径
    dataCleaned.to_csv(os.getcwd()[0:-23] + r"\resources\weiboData\GuangZhou.csv",index=False,encoding="utf_8_sig")
    demo.closeDB()
    print(dataCleaned)

