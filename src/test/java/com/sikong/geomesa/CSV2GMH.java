package com.sikong.geomesa;

import java.io.IOException;
import java.text.ParseException;

public class CSV2GMH {
    public static void main(String[] args) throws IOException, ParseException {
        BatchUpload demo = new BatchUpload();
        demo.batchUpload();
        System.out.println(("Data-ingestion Accomplished!"));
    }
}
