package com.sikong.geomesa;

import lombok.extern.slf4j.Slf4j;
import org.geotools.data.DataStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.util.factory.Hints;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.io.IOException;
import java.util.Date;

@Slf4j
public class Ingest {


    /**
     * 这是一段用于geomesa插入数据的代码示例
     */
    public static void main(String[] args) throws IOException {
        PutFeature demo = new PutFeature();

        /**
         *连接HBase，创建datastore，构建表结构
         */
        DataStore dataStore = demo.mkConnection("ArchiLab");
        SimpleFeatureType sft = demo.setTable("ingestdemo", "Author:String,dtg:Date,*geom:Point:srid=4326,description:String");
        dataStore.createSchema(sft);

        /**
         * 设定具体数据
         */
        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(sft);
        builder.set("Author","stan");
        builder.set("dtg", new Date());
        builder.set("geom","POINT (112.988 28.26)");
        builder.set("description","This is a test2 for geomesa-tutorial");
        builder.featureUserData(Hints.USE_PROVIDED_FID,Boolean.TRUE);
        SimpleFeature feature = builder.buildFeature("test02");

        /**
         * 创建IO流，写入数据
         */
        demo.writeFeature(dataStore,sft,feature);
        log.info("Data-ingestion Successfully Accomplished!");
        demo.writeClose();
    }
}
