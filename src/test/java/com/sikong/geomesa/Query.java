package com.sikong.geomesa;

import org.geotools.data.simple.SimpleFeatureCollection;

import java.io.IOException;

public class Query {
    public static void main(String[] args) throws IOException, InterruptedException {
        GetFeature demo = new GetFeature();

        SimpleFeatureCollection featureType = demo.getFeatureType();
        Object[] objects = featureType.toArray();

        demo.readData(objects,10);
    }
}
