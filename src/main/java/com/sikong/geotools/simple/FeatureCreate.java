package com.sikong.geotools.simple;

import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 * 这是一段用于简单构建Geotools要素的代码
 * 1、通过FeatureTypeBuilder构建featureType，并add表结构
 * 2、通过FeatureBuilder构建feature，并add要素数据
 * 3、通过DataUtilities进行更简单的操作
 *
 * @author 梁超 / stan
 * @since 0.1.0
 * @Date 2022/03/25
 */
public class FeatureCreate {

    /**
     *通过实例化SimpleFeatureTypeBuilder对象，构造要素属性表
     *
     * @return featureType
     */
    public SimpleFeatureType mkFeatureType() {

        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();

        typeBuilder.setName("Flag");//构建要素类名称

        typeBuilder.add("name",String.class);
        typeBuilder.add("classification",Integer.class);
        typeBuilder.add("height",Double.class);

        typeBuilder.setCRS(DefaultGeographicCRS.WGS84);//定义要素类坐标系
        typeBuilder.add("location", Point.class);

        SimpleFeatureType featureType = typeBuilder.buildFeatureType();

        /**
         *  a simple way ====>>>
         *  SimpleFeatureType featureType = DataUtilities.createType("Flag", "name:String,classification:Integer,height:Double,location:Point:srid=4326");
         */

        return featureType;
    }

    /**
     * 1、通过实例化SimpleFeatureBuilder对象，构建featureBuilder
     * 2、利用featureBuilder添加要素数据
     *
     * @param featureType 构建好的要素类
     * @return feature    构建好的要素
     */
    public Feature mkFeature(SimpleFeatureType featureType) {
        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);

        featureBuilder.add("Canada");
        featureBuilder.add(1);
        featureBuilder.add(20.5);

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        Point point = geometryFactory.createPoint(new Coordinate(112, 35));

        featureBuilder.add(point);

        SimpleFeature feature = featureBuilder.buildFeature("fid.1");

        return feature;
    }
}
