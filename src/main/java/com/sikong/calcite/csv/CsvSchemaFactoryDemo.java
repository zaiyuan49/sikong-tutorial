package com.sikong.calcite.csv;

import org.apache.calcite.schema.Schema;
import org.apache.calcite.schema.SchemaFactory;
import org.apache.calcite.schema.SchemaPlus;

import java.io.File;
import java.util.Map;

/**
 * CsvSchema工厂demo
 * @author wangbohong
 * @since 0.1.0
 * @date 2022/03/09 16:26:22
 */
public class CsvSchemaFactoryDemo implements SchemaFactory {

    private static final String DATA_FILE_PATH = "dataFile";
    /**
     * @param schemaPlus ：父节点
     * @param s ：schema的名字
     * @param map ：传入自定义参数
     * @return
     */
    @Override
    public Schema create(SchemaPlus schemaPlus, String s, Map<String, Object> map) {
        return new CsvSchemaDemo(String.valueOf(map.get(DATA_FILE_PATH)));
    }
}
