package com.sikong.configuration;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;

/**
 * 这是一个用来定义常量的类
 * @author 梁超 / stan
 * @since 0.1.0
 * @Date 2022/3/3
 */
public class Constant {
    /**
     * No more description about these constants. =。=
     */
    public static final String HBASE_CATALOG = "hbase.catalog";
    public static final String ZK = "hbase.zookeepers";
    public static final String ZK_HOST = "ArchiLab-development01:2181,ArchiLab-development02:2181,ArchiLab-development03:2181";

    public static final String HADOOP_HOST = "hdfs://ArchiLab-development01:8020";
    public static final String USER_NAME = "root";

    //HBase的配置信息
    private static Configuration CONF () {
        Configuration configuration = HBaseConfiguration.create();
        configuration.set(
                "hbase.zookeeper.quorum",
                "ArchiLab-development01," +
                        "ArchiLab-development02," +
                        "ArchiLab-development03"
        );
        return configuration;
    }
    public static final Configuration CONFIGURATION = CONF();
}
