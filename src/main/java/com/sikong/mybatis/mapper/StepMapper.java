package com.sikong.mybatis.mapper;

import com.sikong.mybatis.pojo.Country;
import org.apache.ibatis.annotations.Param;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 分布查询的Mapper接口
 */
public interface StepMapper {

    /**
     * 分布查询2
     */
    Country getAthleteStepTwo(@Param("id") Integer id);

    /**
     * 一对多 分布查询一
     */
    Country getAthsStepOne(@Param("id") Integer id);
}
