package com.sikong.mybatis.mapper;

import com.sikong.mybatis.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: UserMapper
 */
public interface UserMapper {
    /**
     * 配置文件中namespace与mapper的全类名保持一致
     * 方法名与映射文件中的id一致
     *
     * 表 —— 实体类 —— 接口 —— 映射文件
     */

    /**
     * 添加用户信息
     */
    int insertUser();

    /**
     * 添加用户信息
     * @param user
     * @return 影响行数
     */
    int insertUserWithParam(User user);

    /**
     * 更新用户信息(删除略)
     * @return 变化的行数
     */
    int updateUser();

    /**
     * 查询方法
     * @return 一个User对象
     */
    User getUser();

    /**
     * 根据用户名查询用户信息
     */
    User getUserByUsername(String userName);

    /**
     * 验证用户名密码
     */
    User checkLogin(String userName, String passWord);

    /**
     * 验证用户名密码
     */
    User checkLoginByMap(Map<String, Object> map);

    /**
     * 验证用户名密码(注解)
     */
    User checkLoginByParam(@Param("username") String username, @Param("Password") String password);
}
