package com.sikong.mybatis.mapper;

import com.sikong.mybatis.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 测试动态SQL拼接的mapper接口
 */
public interface DynamicMapper {

    /**
     * 利用if标签拼接SQL
     */
    List<User> getUserByIf(User user) ;

    /**
     * 利用choose when
     */
    List<User> getUserByChoose(User user);

    /**
     * 利用foreach 批量添加
     */
    int insertByList(@Param("users") List<User> users);
}
