package com.sikong.mybatis.mapper;

import com.sikong.mybatis.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 一些特殊SQL Mapper接口
 */
public interface SpecialMapper {

    /**
     * 模糊查询
     */
    List<User> getUserByLike(@Param("username") String username);

    /**
     * 批量删除
     */
    Integer deleteMore(@Param("ids") String ids);

    /**
     * 动态设置表名
     */
    List<User> getUserByTable(@Param("table") String table);
}
