package com.sikong.mybatis.mapper;

import com.sikong.mybatis.pojo.Athlete;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: 使用resultMap的mapper接口
 */
public interface ResultMapper {

    /**
     * 利用resultMap解决字段名与属性不一致的问题
     * @return
     */
    List<Athlete> getAllAthlete();

    /**
     * 分布查询1
     */
    Athlete getAthleteStepOne(@Param("athleteId") Integer athleteId);

    /**
     * 一对多分布查询2
     */
    List<Athlete> getAthsStepTwoResultMap(@Param("id") Integer id);

}
