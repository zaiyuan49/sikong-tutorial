package com.sikong.mybatis.pojo;

import lombok.Data;

import java.util.List;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: Java Bean 对应MySQL中的表 country
 */

@Data
public class Country {

    private Integer id;

    private String name;

    private List<Athlete> athletes;

    public Country() {
    }

    public Country(Integer id, String name, List<Athlete> athletes) {
        this.id = id;
        this.name = name;
        this.athletes = athletes;
    }

}
