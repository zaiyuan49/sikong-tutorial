package com.sikong.mybatis.pojo;

import lombok.Data;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: Java Bean 用户类 对应MySQL表中的t_user
 */

@Data
public class User {

    private Integer user_id;

    private String user_name;

    private String password;

    private String sex;

    public User() {
    }

    public User(Integer user_id, String user_name, String password, String sex) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.password = password;
        this.sex = sex;
    }

}
