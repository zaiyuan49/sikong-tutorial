package com.sikong.mybatis.pojo;

import lombok.Data;

/**
 * Author: Wang Bohong
 * Date: 2022-03-23
 * Since: 0.1.0
 * Description: Java Bean 运动员类 对应表 athlete
 */

@Data
public class Athlete {

    private Integer athleteId;

    private String athleteName;

    private Integer age;

    private String sex;


    private String playField;

    private Country country;

    public Athlete() {
    }

    public Athlete(Integer athleteId, String athleteName, Integer age, String sex, Integer countryId, String playField, Country country) {
        this.athleteId = athleteId;
        this.athleteName = athleteName;
        this.age = age;
        this.sex = sex;
        this.country = country;
        this.playField = playField;
    }

}
