package com.sikong.mybatis.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Author: Wang Bohong
 * Date: 2022-03-22
 * Since: 0.1.0
 * 用于获取SqlSession的工具类
 */
@Slf4j
public class SqlSessionUtils {

    public static SqlSession getSqlSession() {
        SqlSession sqlSession = null;
        try {
            InputStream is = SqlSessionUtils.class.getClassLoader().getResourceAsStream("mybatis-config.xml");
            SqlSessionFactory build = new SqlSessionFactoryBuilder().build(is);
            sqlSession = build.openSession(true);
        } catch (Exception e) {
            log.error("io异常", e);
        }
        return sqlSession;
    }
}
