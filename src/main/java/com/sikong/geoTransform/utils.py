import math
from com.sikong.geoTransform.constant import Constant


class CRSutils:

    @staticmethod
    def transformlat(lon:float,lat:float):
        ret = -100.0 + 2.0 * lon + 3.0 * lat + 0.2 * lat * lat + 0.1 * lon * lat + 0.2 * math.sqrt(abs(lon))
        ret += (20.0 * math.sin(6.0 * lon * Constant.pi) + 20.0 * math.sin(2.0 * lon * Constant.pi)) * 2.0 / 3.0
        ret += (20.0 * math.sin(lat * Constant.pi) + 40.0 * math.sin(lat / 3.0 * Constant.pi)) * 2.0 / 3.0
        ret += (160.0 * math.sin(lat / 12.0 * Constant.pi) + 320 * math.sin(lat * Constant.pi / 30.0)) * 2.0 / 3.0
        return ret

    @staticmethod
    def transformlon(lon:float,lat:float):
        ret = 300.0 + lon + 2.0 * lat + 0.1 * lon * lon + 0.1 * lon * lat + 0.1 * math.sqrt(abs(lon))
        ret += (20.0 * math.sin(6.0 * lon * Constant.pi) + 20.0 * math.sin(2.0 * lon * Constant.pi)) * 2.0 / 3.0
        ret += (20.0 * math.sin(lon * Constant.pi) + 40.0 * math.sin(lon / 3.0 * Constant.pi)) * 2.0 / 3.0
        ret += (150.0 * math.sin(lon / 12.0 * Constant.pi) + 300.0 * math.sin(lon / 30.0 * Constant.pi)) * 2.0 / 3.0
        return ret