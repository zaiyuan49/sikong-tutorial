package com.sikong.constant;

/**
 * 定义一些字符串分隔符
 *
 * @author wangbohong
 * @date 2022-03-09 23:26:22
 * @since 0.1.0
 */
public class StringSpliter {
    public final static String COMMA = ",";
    public final static String DOTE = "\\.";
    public final static String COLON = ":";
}
