package com.sikong.caffine;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;

/**
 * Since the cache is usually used as a container for storing global variables,
 * the singleton mode is adopted here to avoid data synchronization.
 *
 * @see <a href="https://blog.csdn.net/l_dongyang/article/details/108326755">Caffine 入门使用</a>
 * @see <a href="https://blog.csdn.net/mnb65482/article/details/80458571">深入理解单例模式：静态内部类单例原理</a>
 *
 * @author  zaiyuan
 * @date    2022-03-07 23:56:28
 */
public class CacheInstance {

    private CacheInstance() {
    }

    public Cache<String, String> cache = Caffeine.newBuilder()
            .maximumSize(100)
            .expireAfterAccess(100L, TimeUnit.SECONDS)
            .build();

    private static class CacheHolder {
        private static CacheInstance INSTANCE = new CacheInstance();
    }

    public static CacheInstance getInstance() {
        return CacheHolder.INSTANCE;
    }

}
